package com.lechat.repository;

import com.lechat.model.Request;

import java.util.List;

public interface RequestRepository {

    void saveRequest(Request request);
    List<Request> retrieveMyRequestSendList(long myId);
    List<Request> retrieveMyRequestReceiveList(long myId);
    int deleteNonPending();


}
