package com.lechat.repository;

import com.lechat.model.User;

import java.util.List;

public interface UserRepository {

    public void saveUser(com.lechat.model.User user);
//    void saveMessage(Message msg);
//    void saveRequest(Request request);

//    int deleteNonPending();
    List<User> retrieveAll();

    User retrieveUserByUsername(String userName);
    long retrieveUserId(String userName);
    User getUserById(long i);
//    List<Request> retrieveMyRequestSendList(long myId);
//    List<Request> retrieveMyRequestReceiveList(long myId);
//    List<Message> retrieveMySendMail(long myId);
//    List<Message> retrieveMyReceivedMail(long fromId, long fromUserId);
//    void closeEMF();
//


}
