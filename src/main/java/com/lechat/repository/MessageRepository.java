package com.lechat.repository;

import com.lechat.model.Message;

import java.util.List;

public interface MessageRepository {

    void saveMessage(Message msg);
    List<Message> retrieveMySendMail(long myId);
    List<Message> retrieveMyReceivedMail(long fromId, long fromUserId);
    List<Message> retrieveMyConversation(long myId, long fromUserId);

}
