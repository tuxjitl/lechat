package com.lechat.repository.impl;

import com.lechat.model.Message;
import com.lechat.repository.EMF;
import com.lechat.repository.MessageRepository;

import javax.persistence.TypedQuery;
import java.util.List;

public class MessageRepositoryImpl implements MessageRepository {

    @Override
    public void saveMessage(Message msg) {
        EMF.em.getTransaction().begin();
        EMF.em.persist(msg);
        EMF.em.getTransaction().commit();
    }

    @Override
    public List<Message> retrieveMySendMail(long myId) {

        TypedQuery<Message> query = EMF.em.createQuery("SELECT m FROM Message m WHERE m.senderMsg.id = :myId order by m.timestamp desc ", Message.class);
        query.setParameter("myId", myId);
        return  query.setMaxResults(10).getResultList();//10 most recent send msgs will be shown
    }

    @Override
    public List<Message> retrieveMyReceivedMail(long myId, long fromUserId) {

        TypedQuery<Message> query = EMF.em.createQuery("SELECT m FROM Message m WHERE m.receivedMsg.id = :myId AND m.senderMsg.id = :fromUserId order by m.timestamp DESC  ",
                Message.class);
        query.setParameter("myId", myId);
        query.setParameter("fromUserId", fromUserId);
        return query.setMaxResults(5).getResultList();//5 most recent messages will be shown
    }

    @Override
    public List<Message> retrieveMyConversation(long myId, long fromUserId) {

        TypedQuery<Message> query = EMF.em.createQuery(
                "SELECT m FROM Message m WHERE (m.receivedMsg.id = :myId AND m.senderMsg.id = :fromUserId) OR (m.receivedMsg.id = :fromUserId AND m.senderMsg.id = :myId) order by m.timestamp DESC  ",
                Message.class);
        query.setParameter("myId", myId);
        query.setParameter("fromUserId", fromUserId);
        return query.setMaxResults(5).getResultList();//5 most recent messages will be shown
    }

}
