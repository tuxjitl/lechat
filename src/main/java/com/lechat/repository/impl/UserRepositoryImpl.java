package com.lechat.repository.impl;

import com.lechat.model.User;
import com.lechat.repository.UserRepository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import java.util.List;

public class UserRepositoryImpl implements UserRepository {

    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("lechat");
    private EntityManager em = emf.createEntityManager();

    @Override
    public void saveUser(com.lechat.model.User user) {

        em.getTransaction().begin();
        em.persist(user);
        em.getTransaction().commit();
    }

    @Override
    public List<User> retrieveAll() {

        TypedQuery<com.lechat.model.User> query = em.createQuery("SELECT u FROM User u", com.lechat.model.User.class);
        return query.getResultList();
    }

    @Override
    public User retrieveUserByUsername(String userName) {

        TypedQuery<com.lechat.model.User> query = em.createQuery("SELECT u FROM User u WHERE u.userName = :userName ", com.lechat.model.User.class);
        query.setParameter("userName", userName);
        //username = unique
        return query.getSingleResult();

    }

    @Override
    public long retrieveUserId(String userName) {

        TypedQuery<Long> query = em.createQuery("SELECT u.id FROM User u WHERE u.userName = :userName ", Long.class);
        query.setParameter("userName", userName);

        return query.getSingleResult();

    }

    @Override
    public User getUserById(long id) {

        TypedQuery<com.lechat.model.User> query = em.createQuery("SELECT u FROM User u WHERE u.id = :id ", com.lechat.model.User.class);
        query.setParameter("id", id);

        return query.getSingleResult();
    }



}
