package com.lechat.repository.impl;

import com.lechat.model.Request;
import com.lechat.repository.EMF;
import com.lechat.repository.RequestRepository;
import com.lechat.repository.RequestState;

import javax.persistence.TypedQuery;
import java.util.List;

public class RequestRepositoryImpl implements RequestRepository {


    @Override
    public void saveRequest(Request request) {

        EMF.em.getTransaction().begin();
        EMF.em.persist(request);
        EMF.em.getTransaction().commit();
    }

    @Override
    public List<Request> retrieveMyRequestSendList(long myId) {

        TypedQuery<Request> query = EMF.em.createQuery("SELECT r FROM Request r WHERE r.sender.id = :myId ", Request.class);
        query.setParameter("myId", myId);
        return query.getResultList();
    }

    @Override
    public List<Request> retrieveMyRequestReceiveList(long myId) {

        TypedQuery<Request> query = EMF.em.createQuery("SELECT r FROM Request r WHERE r.receiver.id = :myId ", Request.class);
        query.setParameter("myId", myId);
        return query.getResultList();
    }

    @Override
    public int deleteNonPending() {

        EMF.em.getTransaction().begin();
        TypedQuery<Request> query = EMF.em.createQuery("SELECT r FROM Request r WHERE r.requestState = :requestState",Request.class);
        query.setParameter("requestState", RequestState.ACCEPTED);
        int rowsDeleted=0;

        for (Request req : query.getResultList()) {
            EMF.em.remove(req);
            rowsDeleted++;

        }

        EMF.em.getTransaction().commit();

        return rowsDeleted;
    }



}
