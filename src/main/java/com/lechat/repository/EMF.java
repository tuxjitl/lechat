package com.lechat.repository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class EMF {

    public static EntityManagerFactory emf = Persistence.createEntityManagerFactory("lechat");
    public static EntityManager em = emf.createEntityManager();

    public static void close() {
//        em.close();
        emf.close();
    }

}
