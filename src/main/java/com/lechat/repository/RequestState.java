package com.lechat.repository;

public enum RequestState {
    PENDING,
    ACCEPTED,
    REJECTED
}
