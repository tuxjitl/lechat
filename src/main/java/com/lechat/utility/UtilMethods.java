package com.lechat.utility;

import org.mindrot.jbcrypt.BCrypt;

import java.util.ArrayList;
import java.util.Arrays;

public class UtilMethods {

    public static String[] getEnumNames(Class<? extends Enum<?>> e) {

        return Arrays.stream(e.getEnumConstants()).map(Enum::name).toArray(String[]::new);
    }

    public static String createHashPassword(String password) {
        // Hash a password
        String hashed = BCrypt.hashpw(password, BCrypt.gensalt());
        return hashed;
    }

    public static boolean checkEncryptedPassword(String candidate, String hashed) {
        // Check that an unencrypted password matches or not
        if (BCrypt.checkpw(candidate, hashed)) {
            return true;
        }
        else {
            return false;
        }
    }

    public static boolean checkForValidPasswordEntry(ArrayList<String> userData) {

        String pw = userData.get(2);
        String pwConfirm = userData.get(3);
        String regExprPasswordFormat = "^(?=.*?[A-Za-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-_]).{12,31}$";

        if (pw.equals(pwConfirm) && pw.matches(regExprPasswordFormat)) {
            return true;
        }

        return false;
    }

    public static boolean checkForLengthUsernameEntry(String s) {

        if (s.length() >= 32) {
            return false;
        }
        return true;
    }

    public static boolean isValidInputString(String s) {

        return s.matches("^[a-zA-Z0-9]*$");

    }

}
