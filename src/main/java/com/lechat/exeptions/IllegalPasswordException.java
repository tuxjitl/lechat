package com.lechat.exeptions;

public class IllegalPasswordException extends Throwable {
    private String message;

    public IllegalPasswordException(String s) {
        this.message = s;
    }


    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
