package com.lechat.exeptions;

public class IllegalEmailException extends Throwable {

    private String message;

    public IllegalEmailException(String s) {
        this.message = s;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
