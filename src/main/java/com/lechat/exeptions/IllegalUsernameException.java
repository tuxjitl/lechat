package com.lechat.exeptions;

public class IllegalUsernameException extends Throwable{

    private String message;

    public IllegalUsernameException(String s) {
        this.message = s;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
