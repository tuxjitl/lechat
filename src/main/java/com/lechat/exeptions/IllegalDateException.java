package com.lechat.exeptions;

public class IllegalDateException extends Throwable {

    private String message;


    public IllegalDateException(String s) {
        this.message = s;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
