package com.lechat.exeptions;

public class UserNotFoundException extends Throwable{

    private String message;

    public UserNotFoundException(String s) {
        this.message = s;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
