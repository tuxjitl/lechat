package com.lechat.model;

import com.lechat.repository.TypeOfMessage;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "message")
public class Message {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @ManyToOne
    private User senderMsg;
    @ManyToOne
    private User receivedMsg;
    @Column(name = "message",columnDefinition="TEXT")
    private String message;
    private LocalDateTime timestamp;
    @Enumerated
    @Column(name = "msg_type")
    private TypeOfMessage typeOfMessage;

    public Message(User senderMsg, User receivedMsg, String message, LocalDateTime timestamp, TypeOfMessage typeOfMessage) {

        this.senderMsg = senderMsg;
        this.receivedMsg = receivedMsg;
        this.message = message;
        this.timestamp = timestamp;
        this.typeOfMessage = typeOfMessage;
    }

    public Message(){

    }

    public long getId() {

        return id;
    }

    public User getSenderMsg() {

        return senderMsg;
    }

    public void setSenderMsg(User senderMsg) {

        this.senderMsg = senderMsg;
    }

    public User getReceivedMsg() {

        return receivedMsg;
    }

    public void setReceivedMsg(User receiverMsg) {

        this.receivedMsg = receiverMsg;
    }

    public String getMessage() {

        return message;
    }

    public void setMessage(String message) {

        this.message = message;
    }

    public LocalDateTime getTimestamp() {

        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {

        this.timestamp = timestamp;
    }

    public TypeOfMessage getTypeOfMessage() {

        return typeOfMessage;
    }

    public void setTypeOfMessage(TypeOfMessage typeOfMessage) {

        this.typeOfMessage = typeOfMessage;
    }

    @Override
    public String toString() {

        final StringBuilder sb = new StringBuilder("Message:");
        sb.append(", timestamp: ").append(timestamp);
        sb.append("\nsenderMsg: ").append(senderMsg);
        sb.append("\nreceivedMsg=").append(receivedMsg);
        sb.append("\nmessage='").append(message).append('\'');
        sb.append('\n');
        return sb.toString();
    }
}
