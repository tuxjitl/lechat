package com.lechat.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(
        name = "user",
        uniqueConstraints = {@UniqueConstraint(columnNames = {"user_name", "email"})})

public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "user_name", length = 32)
    private String userName;
    @Column(length = 50)
    private String email;
    @Column(length = 255)
    private String password;
    @ManyToMany
    @JoinTable(name = "friendship",
            joinColumns = @JoinColumn(name = "user_id", foreignKey = @ForeignKey(name = "Fk_user_id")),
            inverseJoinColumns = @JoinColumn(name = "friend_with", foreignKey = @ForeignKey(name = "Fk_friend_with")))
    private List<User> users = new ArrayList<>();

    @OneToMany(mappedBy = "receiver", fetch = FetchType.LAZY)
    private List<Request> receivedRequests = new ArrayList<>();

    @OneToMany(mappedBy = "sender", fetch = FetchType.LAZY)
    private List<Request> getRequests = new ArrayList<>();

    @OneToMany(mappedBy = "receivedMsg", fetch = FetchType.LAZY)
    private List<Message> receivedMessages = new ArrayList<>();

    @OneToMany(mappedBy = "senderMsg", fetch = FetchType.LAZY)
    private List<Message> sendMessages = new ArrayList<>();


    public User(String userName, String email, String password) {

        this.userName = userName;
        this.email = email;
        this.password = password;
    }

    public User(String userName, String email, String password, List<User> users, List<Request> receivedRequests,
                List<Request> getRequests, List<Message> receivedMessages, List<Message> sendMessages) {

        this.userName = userName;
        this.email = email;
        this.password = password;
        this.users = users;
        this.receivedRequests = receivedRequests;
        this.getRequests = getRequests;
        this.receivedMessages = receivedMessages;
        this.sendMessages = sendMessages;
    }

    public User() {


    }


    public long getId() {

        return id;
    }

    public String getUserName() {

        return userName;
    }

    public void setUserName(String userName) {

        this.userName = userName;
    }

    public String getEmail() {

        return email;
    }

    public void setEmail(String email) {

        this.email = email;
    }

    public String getPassword() {

        return password;
    }

    public void setPassword(String password) {

        this.password = password;
    }

    public List<User> getUsers() {

        return users;
    }

    public void setUsers(List<User> users) {

        this.users = users;
    }

    public List<Request> getReceivedRequests() {

        return receivedRequests;
    }

    public void setReceivedRequests(List<Request> receivedRequests) {

        this.receivedRequests = receivedRequests;
    }

    public List<Request> getGetRequests() {

        return getRequests;
    }

    public void setGetRequests(List<Request> getRequests) {

        this.getRequests = getRequests;
    }

    public List<Message> getReceivedMessages() {

        return receivedMessages;
    }

    public void setReceivedMessages(List<Message> receivedMessages) {

        this.receivedMessages = receivedMessages;
    }

    public List<Message> getSendMessages() {

        return sendMessages;
    }

    public void setSendMessages(List<Message> sendMessages) {

        this.sendMessages = sendMessages;
    }

    @Override
    public String toString() {

        final StringBuilder sb = new StringBuilder("User: ");
        sb.append(" userName: '").append(userName);
        sb.append('\n');
        return sb.toString();
    }
}
