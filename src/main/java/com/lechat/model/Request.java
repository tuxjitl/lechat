package com.lechat.model;

import com.lechat.repository.RequestState;
import com.lechat.repository.TypeOfMessage;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "request")
public class Request {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @ManyToOne
    private User sender;
    @ManyToOne
    private User receiver;
    private LocalDateTime timestamp;
    @Enumerated
    @Column(name = "msg_type")
    private TypeOfMessage typeOfMessage;
    @Enumerated
    @Column(name = "request_state")
    private RequestState requestState;


    public Request(User sender, User recipient, LocalDateTime timestamp, TypeOfMessage typeOfMessage, RequestState requestState) {

        this.sender = sender;
        this.receiver = recipient;
        this.timestamp = timestamp;
        this.typeOfMessage = typeOfMessage;
        this.requestState = requestState;
    }

    public Request(){

    }

    public long getId() {

        return id;
    }

    public User getSender() {

        return sender;
    }

    public void setSender(User sender) {

        this.sender = sender;
    }

    public User getReceiver() {

        return receiver;
    }

    public void setReceiver(User recipient) {

        this.receiver = recipient;
    }

    public LocalDateTime getTimestamp() {

        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {

        this.timestamp = timestamp;
    }

    public TypeOfMessage getTypeOfMessage() {

        return typeOfMessage;
    }

    public void setTypeOfMessage(TypeOfMessage typeOfMessage) {

        this.typeOfMessage = typeOfMessage;
    }

    public RequestState getRequestState() {

        return requestState;
    }

    public void setRequestState(RequestState requestState) {

        this.requestState = requestState;
    }

    @Override
    public String toString() {

        final StringBuilder sb = new StringBuilder("Request: ");
        sb.append(" timestamp: ").append(timestamp);
        sb.append("\nsender:").append(sender);
        sb.append("\nreceiver: ").append(receiver);
        sb.append("\nrequestState: ").append(requestState);
        sb.append('\n');
        return sb.toString();
    }
}
