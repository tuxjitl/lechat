package com.lechat.service;

import com.lechat.model.Message;
import com.lechat.model.User;

import java.util.List;

public interface MessageService {

    void saveMessage(Message msg);
    List<Message> retrieveMySendMail(long myId);
    List<Message> retrieveMyReceivedMail(long fromId, long fromUserId);
    void showMessages(List<Message> myMessages, String s, String msgMessage, User me);
    void ViewSendMessages(User me, String[] myFriends);
    void sendMessageTo(User me, String[] myFriends);
    void ViewIncomingMessages(User me, String[] myFriends);
    void ViewConversation(User me, String[] myFriends);
    List<Message> retrieveMyConversation(long fromId, long fromUserId);
    void showConversation(List<Message> myMessages, String s, String msgMessage, User me);

}
