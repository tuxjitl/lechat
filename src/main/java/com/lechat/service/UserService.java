package com.lechat.service;

import com.lechat.model.Request;
import com.lechat.model.User;

import java.util.ArrayList;
import java.util.List;

public interface UserService {

    void saveUser(com.lechat.model.User user);
    List<User> retrieveAll();
    User retrieveUserByUsername(String userName);
    long retrieveUserId(String userName);
    User getUserById(long i);
    boolean validateUser(String loginUsername, String loginPassword);
    boolean checkForUniqueness(ArrayList<String> userData, List<User> toCheckUserData);
    boolean ValidationNewUserData(ArrayList<String> userData);
    void viewFriendsList(Long userId);
    List<String> convertUserListToStringList(List<User> aList, long userId);
    List<User> removeMeAndFriendsFromUserList(long userId, List<User> userList);
    List<String> filterUserList(List<Request> usersFromRequests, long userId);
    List<User> getRequestSendToUsers(List<Request> mySendRequests, long userId);

}
