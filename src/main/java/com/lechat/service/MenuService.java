package com.lechat.service;

public interface MenuService {

    void start();
    void stop();
    void quit();
    void mainMenu();
    void registerUser();
    void login();
}
