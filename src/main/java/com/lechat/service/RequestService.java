package com.lechat.service;

import com.lechat.model.Request;
import com.lechat.model.User;

import java.util.List;

public interface RequestService {

    List<Request> retrieveMyRequestSendList(Long userId);
    List<Request> retrieveMyRequestReceiveList(long myId);
    void saveRequest(Request request);
    int deleteNonPending();
    boolean checkEmptyList(List<Request> myList, User me);
    void showSendRequestList(User me, List<String> listRequestSendToUsers);
    String[] convertListToStringList(List<Request> aRequestList);
    void sendRequestForFriendship(long userId);
    void viewRequestsReceived(long id);
    void viewRequestsSend(long userId);

}
