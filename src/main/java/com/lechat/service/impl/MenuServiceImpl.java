package com.lechat.service.impl;

import com.lechat.model.User;
import com.lechat.repository.EMF;
import com.lechat.service.MenuService;
import com.lechat.service.MessageService;
import com.lechat.service.RequestService;
import com.lechat.service.UserService;
import com.lechat.utility.UtilMethods;
import org.tuxjitl.keyboardutility.keyboardinput.KeyboardUtility;
import org.tuxjitl.keyboardutility.menudecoration.MenuUtility;

import java.util.ArrayList;
import java.util.List;

public class MenuServiceImpl implements MenuService {

    private UserService userService = new UserServiceImpl();
    private MessageService msgService = new MessageServiceImpl();
    private RequestService requestService = new RequestServiceImpl();

    @Override
    public void start() {

        menuHeader("LeChat chat", "Home Menu");
        mainMenu();
    }

    @Override
    public void stop() {

        System.out.println("Closing Entity Manager Factory");
        EMF.emf.close();
    }

    @Override
    public void quit() {

        System.out.println(MenuUtility.doubleThinLine());
        System.out.println(MenuUtility.center("Goodbye"));
        System.out.println(MenuUtility.center("Leaving the application"));
        System.exit(0);

    }

    @Override
    public void mainMenu() {

        boolean answer = true;

        do {
            int choice = KeyboardUtility.askForChoice("Make a choice", MenuData.MAIN_ITEMS);
            switch (choice) {
                case 0:
                    registerUser();
                    break;
                case 1:
                    login();
                    break;
                case 2:
                    quit();
                default:
                    break;
            }

            answer = KeyboardUtility.askYOrN("Do you want to continue to use LeChat?");
        } while (answer);
        stop();
    }

    @Override
    public void login() {

        String loginUsername = KeyboardUtility.ask("Enter your username");
        String loginPassword = KeyboardUtility.ask("Enter your password");
        boolean isRegistered = userService.validateUser(loginUsername, loginPassword);
        if (isRegistered) {
            long userId = userService.retrieveUserId(loginUsername);
            showLogin(loginUsername, userId);
        }
        else {
            System.out.println("User validation failed. Perhaps you want to register first?");
        }
    }

    @Override
    public void registerUser() {

        menuHeader("LeChar Registry", "Welcome, o Obi-Wan");
        boolean isCorrectData = true;

        ArrayList<String> userData = new ArrayList<>();

        reminderEntry();
        while (isCorrectData) {
            userData.add(KeyboardUtility.ask("Enter username"));
            userData.add(KeyboardUtility.askForEmail("Enter email"));
            userData.add(KeyboardUtility.ask("Enter password"));
            userData.add(KeyboardUtility.ask("Confirm password"));

            isCorrectData = userService.ValidationNewUserData(userData);
            if (isCorrectData) {
                //prepare userData for hashed pw
                userData.remove(userData.size() - 1);
                String password = userData.get(userData.size() - 1);

                String hashed = UtilMethods.createHashPassword(password);

                userData.set(userData.size() - 1, hashed);

                User newUser = new com.lechat.model.User(userData.get(0), userData.get(1), userData.get(2));
                userService.saveUser(newUser);
                System.out.println("User saved");
                userData.clear();
                boolean answer = KeyboardUtility.askYOrN("Add another account?");
                if (!answer) {
                    isCorrectData = !isCorrectData;
                }
            }
            else {
                isCorrectData = !isCorrectData;
                userData.clear();
                System.out.println("User data incorrect");
                reminderEntry();
            }
        }


    }

    private void menuHeader(String strMenuMessage, String strMsg) {

        System.out.println(MenuUtility.doubleThinLine());
        System.out.println();
        System.out.println(MenuUtility.center(strMenuMessage));
        System.out.println();
        System.out.println(MenuUtility.center(strMsg));
        System.out.println(MenuUtility.doubleThinLine());
        System.out.println();

    }

    private void showLogin(String loginUsername, long userId) {

        menuHeader("Welcome to LeChat," + loginUsername, "Chat Menu");
        boolean isCorrect = true;
        while (isCorrect) {
            int choice = KeyboardUtility.askForChoice("Choose an option", MenuData.CHAT_MENU_ITEMS);

            switch (choice) {
                case 0:
                    userService.viewFriendsList(userId);
                    break;
                case 1:
                    chat(userId);
                    break;
                case 2:
                    requestService.sendRequestForFriendship(userId);
                    break;
                case 3:
                    requestService.viewRequestsReceived(userId);
                    cleanupRequests();
                    break;
                case 4:
                    requestService.viewRequestsSend(userId);
                    break;
                case 5:
                    System.out.println("Logging out. See you next time.");
                    isCorrect = false;
                    break;
                default:
                    break;
            }
        }
    }

    private void reminderEntry() {

        System.out.println();
        System.out.println(MenuUtility.thinLine());
        System.out.println();
        System.out.println(MenuUtility.center("NOTE"));
        System.out.println();
        System.out.println("Username must be unique and max 31 long.");
        System.out.println("Username can only consist of alphanumeric characters.");
        System.out.println("Email must be unique.");
        System.out.println("Email must be like somebody@somewhere.topleveldomain.");
        System.out.println("Password must be between [12-31] character long.");
        System.out.println();
        System.out.println(MenuUtility.thinLine());
    }

    private void cleanupRequests() {

        int rowsDeleted = requestService.deleteNonPending();
        System.out.println(rowsDeleted + " records cleaned up in Request tabel");
        System.out.println();
    }

    private void chat(long userId) {

        User me = userService.getUserById(userId);
        List<User> myUserFriendsList = me.getUsers();

        List<String> myFriendsList =userService.convertUserListToStringList(myUserFriendsList, userId);
        myFriendsList.add("Quit Messages");
        String[] myFriends = myFriendsList.toArray(new String[0]);

        boolean sendMsg = true;
        while (sendMsg) {
            int choice = KeyboardUtility.askForChoice("Choose action", MenuData.CHAT_SEND_VIEW_MSG);

            switch (choice) {
                case 0:
                    msgService.sendMessageTo(me, myFriends);
                    break;
                case 1:
                    msgService.ViewConversation(me, myFriends);
                    break;
                case 2:
                    msgService.ViewIncomingMessages(me, myFriends);
                    break;
                case 3:
                    msgService.ViewSendMessages(me, myFriends);
                    break;
                default:
                    return;
            }
        }


    }



}
