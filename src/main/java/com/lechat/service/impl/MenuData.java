package com.lechat.service.impl;

public class MenuData {

    static final String[] MAIN_ITEMS = {"Register","Log In","Exit Application"};
    static final String[] CHAT_MENU_ITEMS={"View My Friends","Chat","Send Request For Friendship",
            "View/Approve Received Requests",
            "View My Send Requests","Log out"};
    static final String[] CHAT_SEND_VIEW_MSG = {"Send Message","View conversation","View Messages Received","View Messages Send","Return to Login Menu"};
}
