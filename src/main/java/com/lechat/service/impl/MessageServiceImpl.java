package com.lechat.service.impl;

import com.lechat.model.Message;
import com.lechat.model.User;
import com.lechat.repository.MessageRepository;
import com.lechat.repository.TypeOfMessage;
import com.lechat.repository.impl.MessageRepositoryImpl;
import com.lechat.service.MessageService;
import com.lechat.service.UserService;
import org.tuxjitl.keyboardutility.keyboardinput.KeyboardUtility;
import org.tuxjitl.keyboardutility.menudecoration.MenuUtility;

import java.time.LocalDateTime;
import java.util.List;

public class MessageServiceImpl implements MessageService {

    private final MessageRepository msgRepo = new MessageRepositoryImpl();
    private final UserService userService = new UserServiceImpl();

    @Override
    public void saveMessage(Message msg) {
        msgRepo.saveMessage(msg);
    }

    @Override
    public List<Message> retrieveMySendMail(long myId) {

        return msgRepo.retrieveMySendMail(myId);
    }

    @Override
    public List<Message> retrieveMyReceivedMail(long fromId, long fromUserId) {

        return msgRepo.retrieveMyReceivedMail(fromId,fromUserId);
    }

    @Override
    public void showMessages(List<Message> myMessages, String s, String msgMessage, User me) {

        System.out.println();
        System.out.println(MenuUtility.thinLine());
        System.out.println(msgMessage + me.getUserName());

        if (myMessages.isEmpty()) {
            System.out.println(s);
            System.out.println();
            System.out.println(MenuUtility.doubleThinLine());
            System.out.println();
            return;

        }
        System.out.println();
        myMessages.stream().forEach(System.out::println);
        System.out.println(MenuUtility.doubleThinLine());
        System.out.println();
        System.out.println();
    }

    @Override
    public void showConversation(List<Message> myMessages, String s, String msgMessage, User me) {

        System.out.println();
        System.out.println(MenuUtility.thinLine());
        System.out.println(msgMessage + me.getUserName());

        if (myMessages.isEmpty()) {
            System.out.println(s);
            System.out.println();
            System.out.println(MenuUtility.doubleThinLine());
            System.out.println();
            return;

        }
        System.out.println();
        myMessages.stream().forEach(System.out::println);
        System.out.println(MenuUtility.doubleThinLine());
        System.out.println();
        System.out.println();
    }

    @Override
    public void ViewSendMessages(User me, String[] myFriends) {

        long myId = me.getId();

        List<Message> myMessages = retrieveMySendMail(myId);
        showMessages(myMessages, "No messages send", "5 Most Recent Msg Send From ", me);

    }

    @Override
    public void sendMessageTo(User me, String[] myFriends) {

        int choice = KeyboardUtility.askForChoice("Send message to ", myFriends);
        if (choice == myFriends.length - 1) {
            return;
        }

        User sendToUser = userService.retrieveUserByUsername(myFriends[choice]);
        String prefixTo = sendToUser.getUserName();

        String msg = KeyboardUtility.ask("Enter message");

        Message message = new Message(me, sendToUser, msg, LocalDateTime.now(), TypeOfMessage.MESSAGE);
        saveMessage(message);
        System.out.println("Message send to " + sendToUser.getUserName());

        System.out.println(MenuUtility.doubleThinLine());
        System.out.println();
    }

    @Override
    public void ViewIncomingMessages(User me, String[] myFriends) {

        long myId = me.getId();
        int choice = KeyboardUtility.askForChoice("See message from ", myFriends);
        if(choice == myFriends.length-1){
            return;
        }
        long fromUserId = userService.retrieveUserByUsername(myFriends[choice]).getId();
        List<Message> myMessages = retrieveMyReceivedMail(myId, fromUserId);
        showMessages(myMessages, "No messages received from " + myFriends[choice], "5 Most Recent Msg received For ", me);

    }

    @Override
    public void ViewConversation(User me, String[] myFriends) {

        long myId = me.getId();
        int choice = KeyboardUtility.askForChoice("See conversation with", myFriends);
        long fromUserId = userService.retrieveUserByUsername(myFriends[choice]).getId();
        List<Message> myMessages = retrieveMyConversation(myId, fromUserId);
        showConversation(myMessages, "No messages received from " + myFriends[choice], "5 Most Recent messages in conversation ", me);

    }

    @Override
    public List<Message> retrieveMyConversation(long fromId, long fromUserId) {

        return msgRepo.retrieveMyConversation(fromId,fromUserId);
    }

}
