package com.lechat.service.impl;

import com.lechat.model.Request;
import com.lechat.model.User;
import com.lechat.repository.RequestRepository;
import com.lechat.repository.UserRepository;
import com.lechat.repository.impl.RequestRepositoryImpl;
import com.lechat.repository.impl.UserRepositoryImpl;
import com.lechat.service.UserService;
import com.lechat.utility.UtilMethods;
import org.tuxjitl.keyboardutility.menudecoration.MenuUtility;

import java.util.ArrayList;
import java.util.List;

public class UserServiceImpl implements UserService {

    private final UserRepository userRepository = new UserRepositoryImpl();
    private final RequestRepository requestRepository = new RequestRepositoryImpl();

    @Override
    public void saveUser(User user) {

        userRepository.saveUser(user);
    }

    @Override
    public List<User> retrieveAll() {

        return userRepository.retrieveAll();
    }

    @Override
    public User retrieveUserByUsername(String userName) {

        return userRepository.retrieveUserByUsername(userName);
    }

    @Override
    public long retrieveUserId(String userName) {

        return userRepository.retrieveUserId(userName);
    }

    @Override
    public User getUserById(long userId) {

        return userRepository.getUserById(userId);
    }

    @Override
    public boolean validateUser(String loginUsername, String loginPassword) {

        List<User> userList;
        userList = retrieveAll();

        boolean isRegisteredUser = false;
        for (User user : userList) {
            if (user.getUserName().equals(loginUsername)) {
                isRegisteredUser = UtilMethods.checkEncryptedPassword(loginPassword, user.getPassword());
                break;
            }
        }
        return isRegisteredUser;

    }

    @Override
    public boolean ValidationNewUserData(ArrayList<String> userData) {

        boolean isCorrect = true;
        List<User> toCheckUserData;
        toCheckUserData = retrieveAll();

        if (!isCorrect == UtilMethods.checkForLengthUsernameEntry(userData.get(0))) {
            System.out.println("Username must be at least 12 characters long");
            return false;
        }
        else if (!isCorrect == checkForUniqueness(userData, toCheckUserData)) {
            System.out.println("Username/ email already taken. Please choose others.");
            return false;
        }

        isCorrect = UtilMethods.checkForValidPasswordEntry(userData);

        return isCorrect;
    }

    @Override
    public boolean checkForUniqueness(ArrayList<String> userData, List<User> toCheckUserData) {

        if (toCheckUserData.size() == 0) {
            //no need to validate email because there are no users at this stage
            //and email input is checked when entered by KeyboardUtility
            String s = userData.get(0);
            if (s.length() < 32 && UtilMethods.isValidInputString(s)) {
                return true;
            }
        }
        else {
            String newUserName = userData.get(0);
            String newEmail = userData.get(1);

            for (int i = 0; i < toCheckUserData.size(); i++) {
                User toCheckUser = toCheckUserData.get(i);

                //check both username and email on uniqueness in db
                if (newUserName.equals(toCheckUser.getUserName())) {
                    return false;
                }
                if (newEmail.equals(toCheckUser.getEmail())) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    @Override
    public void viewFriendsList(Long userId) {

        User me = getUserById(userId);
        List<User> myFriends = me.getUsers();

        if (myFriends.size() == 0) {
            System.out.println("No friends to display.");
            System.out.println(MenuUtility.doubleThinLine());
            System.out.println();
            return;
        }

        List<String> myList = convertUserListToStringList(myFriends, userId);

        System.out.println(MenuUtility.doubleThinLine());
        System.out.println("List of friends:");
        myList.stream().forEach(System.out::println);

        System.out.println();
        System.out.println(MenuUtility.thickLine());
        return;

    }

    @Override
    public List<String> convertUserListToStringList(List<User> aList, long userId) {

        List<String> cleanedUpUser = new ArrayList<>();

        for (int i = 0; i < aList.size(); i++) {
            if (aList.get(i).getId() == userId) {
                continue;
            }

            long friendId = aList.get(i).getId();
            User friend = getUserById(friendId);
            String strFriend = friend.getUserName();
            cleanedUpUser.add(strFriend);
        }

        return cleanedUpUser;
    }

    @Override
    public List<User> removeMeAndFriendsFromUserList(long userId, List<User> userList) {

        User me = getUserById(userId);
        List<User> myFriends = me.getUsers();
        for (User friend : myFriends) {

            userList.remove(friend);
        }
        userList.remove(me);
        return userList;
    }

    @Override
    public List<String> filterUserList(List<Request> usersFromRequests, long userId) {

        List<String> cleaned = new ArrayList<>();

        User me = getUserById(userId);

        List<User> listOfUsersSendRequestTo = getRequestSendToUsers(usersFromRequests, userId);

        List<User> allUsersCleaned = retrieveAll();

        for (User reqSendToUser : listOfUsersSendRequestTo) {
            User toRemoveUserFromList = reqSendToUser;
            allUsersCleaned.remove(toRemoveUserFromList);
        }

        allUsersCleaned = removeMeAndFriendsFromUserList(userId, allUsersCleaned);
        allUsersCleaned = removeAlreadySendRequests(me, allUsersCleaned);
        cleaned = convertUserListToStringList(allUsersCleaned, userId);
        cleaned.add("Quit");

        return cleaned;

    }

    private List<User> removeAlreadySendRequests(User me, List<User> allUsersCleaned) {

        List<Request> allreadySendRequests = new ArrayList<>();
        allreadySendRequests = requestRepository.retrieveMyRequestSendList(me.getId());

        for (Request reqSendToUser : allreadySendRequests) {
            User nameToRemoveUserFromList = reqSendToUser.getReceiver();

            User toDelete = retrieveUserByUsername(nameToRemoveUserFromList.getUserName());
            System.out.println(toDelete);
            allUsersCleaned.remove(toDelete);
        }

        return allUsersCleaned;
    }

    @Override
    public List<User> getRequestSendToUsers(List<Request> mySendRequests, long userId) {

        List<User> sendToUsers = new ArrayList<>();

        List<User> sendToUser = new ArrayList<>();

        for (Request req : mySendRequests) {
            sendToUser.add(req.getSender());
        }

        for (User userReq : sendToUser) {
            sendToUsers.add(getUserById(userReq.getId()));
        }

        return sendToUsers;
    }
}
