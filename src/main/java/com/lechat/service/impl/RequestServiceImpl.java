package com.lechat.service.impl;

import com.lechat.model.Request;
import com.lechat.model.User;
import com.lechat.repository.RequestRepository;
import com.lechat.repository.RequestState;
import com.lechat.repository.TypeOfMessage;
import com.lechat.repository.impl.RequestRepositoryImpl;
import com.lechat.service.RequestService;
import com.lechat.service.UserService;
import org.tuxjitl.keyboardutility.keyboardinput.KeyboardUtility;
import org.tuxjitl.keyboardutility.menudecoration.MenuUtility;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class RequestServiceImpl implements RequestService {

    private final RequestRepository reqRepo = new RequestRepositoryImpl();
    private final UserService userService = new UserServiceImpl();

    @Override
    public List<Request> retrieveMyRequestSendList(Long userId) {

        return reqRepo.retrieveMyRequestSendList(userId);
    }

    @Override
    public List<Request> retrieveMyRequestReceiveList(long myId) {

        return reqRepo.retrieveMyRequestReceiveList(myId);
    }

    @Override
    public void saveRequest(Request request) {
        reqRepo.saveRequest(request);
    }

    @Override
    public int deleteNonPending() {

        return reqRepo.deleteNonPending();
    }

    @Override
    public boolean checkEmptyList(List<Request> myList, User me) {

        if (myList.size() == 0) {
            System.out.println(MenuUtility.doubleThinLine());
            System.out.println("\nNo received requests for " + me.getUserName());
            System.out.println("Returning to Login menu.");
            System.out.println(MenuUtility.thinLine() + "\n");
            return true;
        }
        return false;
    }

    @Override
    public void showSendRequestList(User me, List<String> listRequestSendToUsers) {

        System.out.println(MenuUtility.doubleThinLine());
        System.out.println();
        System.out.println("Request send to users by " + me.getUserName());
        System.out.println();
        listRequestSendToUsers.stream().forEach(System.out::println);
        System.out.println();
        System.out.println(MenuUtility.thinLine());
    }

    @Override
    public String[] convertListToStringList(List<Request> aRequestList) {

        String[] received = new String[aRequestList.size() + 1];

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

        for (int i = 0; i < aRequestList.size(); i++) {
            User sender = aRequestList.get(i).getSender();
            User fromUser = userService.getUserById(sender.getId());
            String requestFromName = fromUser.getUserName();

            LocalDateTime timestamp = aRequestList.get(i).getTimestamp();
            String strTimestamp = timestamp.format(formatter);
            RequestState statusRequest = aRequestList.get(i).getRequestState();
            StringBuilder requestReceived = new StringBuilder();
            requestReceived.append("Request from: " + requestFromName).
                    append(", Time: " + strTimestamp).append(", Status: " + statusRequest);

            received[i] = requestReceived.toString();
        }

        received[aRequestList.size()] = "Quit";

        return received;

    }

    @Override
    public void sendRequestForFriendship(long userId) {

        List<Request> usersFromRequests = retrieveMyRequestSendList(userId);

        List<String> requestSendList = userService.filterUserList(usersFromRequests, userId);

        User me = userService.getUserById(userId);

        String[] users = requestSendList.stream().toArray(String[]::new);

        int choice = KeyboardUtility.askForChoice("Which user do you want to make a friend", users);
        if (choice == users.length - 1) {
            return;
        }

        String toBeFriend = users[choice];

        User otherUser = userService.retrieveUserByUsername(toBeFriend);

        //Request comes from me to other user
        Request request = new Request(me, otherUser, LocalDateTime.now(),
                TypeOfMessage.REQUEST, RequestState.PENDING);

        saveRequest(request);

        System.out.println();
        System.out.println("Request send to " + otherUser.getUserName());
        System.out.println();
        System.out.println(MenuUtility.doubleThinLine());
    }

    @Override
    public void viewRequestsReceived(long id) {

        List<Request> myReceivedRequests = retrieveMyRequestReceiveList(id);

        User me = userService.getUserById(id);
        List<Request> myrequests = me.getGetRequests();

        System.out.println(myrequests);

        if (checkEmptyList(myReceivedRequests, me)) {
            return;
        }

        String[] receivedRequests = convertListToStringList(myReceivedRequests);

        int choice = KeyboardUtility.askForChoice("Accept as friend(s)?", receivedRequests);

        if (choice == receivedRequests.length - 1) {
            return;
        }
        else {
            User friendsWith = myReceivedRequests.get(choice).getSender();

            myReceivedRequests.get(choice).setRequestState(RequestState.ACCEPTED);

            me.getUsers().add(friendsWith);

            friendsWith.getUsers().add(me);
            userService.saveUser(me);
        }
    }

    @Override
    public void viewRequestsSend(long userId) {

        User me = userService.getUserById(userId);

        List<Request> mySendRequests = retrieveMyRequestSendList(userId);
        if (mySendRequests.size() == 0) {
            System.out.println();
            System.out.println("No Pending Requests To Show For: " + me.getUserName());
            System.out.println();
            System.out.println(MenuUtility.doubleThinLine());
            System.out.println();
            return;
        }

        List<String> userNamesRequestSendTo = new ArrayList<>();

        for (Request r : mySendRequests) {
            if(r.getReceiver().getUserName().equals(me.getUserName())){
                continue;
            }
            userNamesRequestSendTo.add(r.getReceiver().getUserName());
        }

        showSendRequestList(me, userNamesRequestSendTo);

    }
}
